import json

gamesDict = {'Games': [{'title': 'Super Smash Bros. Ultimate', 'id': 0, 'genre': 'Fighting', 'company': 'Nintendo'}, {'title': 'Grand Theft Auto V', 'id': 1, 'genre': ['Shooter', 'RPG'], 'company': 'Rockstar Games'}, {'title': 'Overwatch', 'id': 2, 'genre': 'Shooter', 'company': 'Blizzard Entertainment'}, {'title': 'Fortnite', 'id': 3, 'genre': 'Shooter', 'company': 'Epic Games'}, {'title': 'World of Warcraft', 'id': 4, 'genre': ['MMO', 'RPG'], 'company': 'Blizzard Entertainment'}, {'title': 'Call of Duty: Black Ops 4', 'id': 5, 'genre': 'Shooter', 'company': 'Treyarch'}, {'title': 'The Legend of Zelda: Breath of the Wild', 'id': 6, 'genre': ['Survival', 'RPG'], 'company': 'Nintendo'}, {'title': 'Mario Kart 8 Deluxe', 'id': 7, 'genre': 'Racing', 'company': 'Nintendo'}, {'title': 'Hearthstone', 'id': 8, 'genre': 'Strategy', 'company': 'Blizzard Entertainment'}, {'title': 'Red Dead Redemption 2', 'id': 9, 'genre': ['Shooter', 'RPG'], 'company': 'Rockstar Games'}, {'title': 'Splatoon 2', 'id': 10, 'genre': 'Shooter', 'company': 'Nintendo'}, {'title': 'Uncahrted 4', 'id': 11, 'genre': ['Adventure', 'Shooter'], 'company': 'Naughty Dog'}, {'title': 'Super Mario Odyssey', 'id': 12, 'genre': 'Platformer', 'company': 'Nintendo'}, {'title': 'Spider-Man', 'id': 13, 'genre': 'Adventure', 'company': 'Insomniac Games'}, {'title': 'God of War', 'id': 14, 'genre': 'Adventure', 'company': 'Santa Monica Studio'}]}

genresDict = {'Genres': [{'title': 'Platformer', 'id': 0, 'games': 'Super Mario Odyssey', 'companies': 'Nintendo'}, {'title': 'Shooter', 'id': 1, 'games': ['Grand Theft Auto V', 'Overwatch', 'Call of Duty: Black Ops 4', 'Red Dead Redemption 2', 'Fortnite'], 'companies': ['Valve Corporation', 'Rockstar Games', 'Electronic Arts', 'Ubisoft']}, {'title': 'Fighting', 'id': 2, 'games': ['Super Smash Bros. Ultimate', 'Mortal Kombat 11'], 'companies': ['Nintendo', 'NetherRealm Studios']}, {'title': 'Stealth', 'id': 3, 'games': ["Assassin's Creed Odyssey", 'Metal Gear Solid V: Phantom Pain'], 'companies': ['Ubisoft', 'Konami']},{'title': 'Survival', 'id': 4, 'games': ['The Legend of Zelda: Breath of the Wild', 'Fallout 3'], 'companies': ['Nintendo', 'Bethesda Softworks']}, {'title': 'Rhythm', 'id': 5, 'games': ['Guitar Hero 3', 'Rock Band 4'], 'companies': ['Neversoft', 'Harmonix Music Systems']}]}

companiesDict = {'Companies': [{'title': 'Nintendo', 'id': 0, 'games': ['Super Smash Bros. Ultimate', 'The Legend of Zelda: Breath of the Wild'], 'genres': ['Platformer', 'Adventure']}, {'title': 'Valve Corporation', 'id': 1, 'games': ['Half-Life', 'Portal', 'Team Fortress 2'], 'genres': ['Shooter', 'Adventure']}, {'title': 'Rockstar Games', 'id': 2, 'games': ['Grand Theft Auto V', 'Red Dead Redemption 2'], 'genres': ['RPG', 'Shooter']}, {'title': 'Electronic Arts', 'id': 3, 'games': ['Apex Legends', 'FIFA 19', 'Anthem'], 'genres': ['Shooter', 'Sports']}, {'title': 'Blizzard Entertainment', 'id': 4, 'games': ['World of Warcraft', 'Hearthstone'], 'genres': ['MMO', 'Strategy']}, {'title': 'Ubisoft', 'id': 5, 'games': ["Asassin's Creed Odyssey", 'The Division 2', 'The Crew 2'], 'genres': ['Adventure', 'Shooter']}]}


with open ("games.json", "w") as fp:
	json.dump(gamesDict, fp, indent = 4)



with open ("genres.json", "w") as fp:
	json.dump(genresDict, fp, indent = 4)



with open ("companies.json", "w") as fp:
	json.dump(companiesDict, fp, indent = 4)

